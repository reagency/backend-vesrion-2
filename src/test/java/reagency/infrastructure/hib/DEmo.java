package reagency.infrastructure.hib;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.HibernateUtility;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.LocationViewModel;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.Country;
import reagency.core.domain.realestate.areaaggregate.District;
import reagency.core.domain.realestate.areaaggregate.State;

public class DEmo {

	@Test
	public void test() {
		
		Country country1 = new Country();
		country1.setName("Canada");
		State state1 = new State();
		state1.setName("QC");
		City city1 = new City();
		city1.setName("Montreal");
		District ds1 = new District();
		ds1.setName("NDG");
		city1.addDistrict(ds1);
		District ds2 = new District();
		ds2.setName("downtown");
		city1.addDistrict(ds2);
		District ds3 = new District();
		ds3.setName("cote des neiges");
		city1.addDistrict(ds3);
		state1.addCity(city1);
		State state2 = new State();
		state2.setName("BC");
		
		country1.addState(state1);	
		country1.addState(state2);
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		uow.getCountryRepository().save(country1);
		uow.commit();
		context.close();
//		State s1 = new State();
//		s1.setName("QC");
//		s1.setCities(cities);
//		country.setName("Canada");
//		country.setStates(states);
	}
	@Test
	public void test2(){
//		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
//				DIConfiguration.class);
//		IUnitOfWork uow = context.getBean(UnitOfWork.class);
//		
//		Country country = uow.getCountryRepository().findById(1);
//		List<LocationViewModel> result = new ArrayList<>();
//		if(country != null){
//			int size = country.getStates().size();
//			List<State> states = country.getStates();
//			for (State state : states) {
//				System.out.println(state.getName());
//			}
//		}
	}

}
