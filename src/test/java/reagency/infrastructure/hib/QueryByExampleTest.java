package reagency.infrastructure.hib;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.HibernateUtility;
import reagency.infrastructure.hib.UnitOfWork;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.estateaggregate.Estate;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;

public class QueryByExampleTest {

	@Test
	public void test() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		SellingOffer sellingOffer = new SellingOffer();
//		SellingOfferType sellingOfferType = (SellingOfferType)uow.getrETypeRepository().findById(7);
//		sellingOffer.setType(sellingOfferType);
//		List<SellingOffer> result = uow.getSellingOfferRepository().findByExample(sellingOffer);
//		System.out.println(result.size());
		Session session = HibernateUtility.getSessionFactory().openSession();
		sellingOffer.setBasePrice(1000);
		Estate estate = new Estate();
		//estate.setAge(15);
		//sellingOffer.setEstate(estate);
		
		Example example = Example.create(sellingOffer);

		Criteria criteria4 = session.createCriteria(SellingOffer.class)
				.add(example);
		List<SellingOffer> lsList = criteria4.list();
		for (SellingOffer sellingOffer2 : lsList) {
			Estate e= sellingOffer2.getEstate();
			System.out.println(e.getAge());
		}
		System.out.println(lsList.size());
	}

}
