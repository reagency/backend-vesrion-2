package reagency.infrastructure.security;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.bind.annotation.*;
import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.User;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.LoggedInUser;

import javax.servlet.ServletException;
import java.util.*;

@RestController
@RequestMapping(value = "user")
public class UserController {

    @RequestMapping(value = "newuser", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public String addNewUser(@RequestBody User newUser){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                DIConfiguration.class);
        IUnitOfWork uow = context.getBean(UnitOfWork.class);

        boolean isDone = uow.getUserRepository().save(newUser);
        uow.commit();
        context.close();
        return (new Boolean(isDone).toString());
    }

    @RequestMapping(value = "check/{username}", method = RequestMethod.GET, produces = "text/plain")
    public String checkUserExist(@PathVariable("username") String username){
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                DIConfiguration.class);
        IUnitOfWork uow = context.getBean(UnitOfWork.class);
        User user_example = new User();
        user_example.setUsername(username);
        List<User> users = new ArrayList<>();
        users = uow.getUserRepository().getUserByUsername(username);
        return (users.size() > 0 ? "true" : "false");
    }

    @RequestMapping(value="/login", method=RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public LoggedInUser login(@RequestBody Map<String, String> json) throws ServletException {

        if (json.get("username")==null || json.get("password") == null) {
            throw new ServletException ("Please fill in username and password");
        }

        String username =json.get("username");
        String password = json.get("password");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                DIConfiguration.class);
        IUnitOfWork uow = context.getBean(UnitOfWork.class);
        List<User> users = uow.getUserRepository().getUserByUsername(username);
        User user = null;
        if(users != null){
            if(users.size() == 0){
                throw new ServletException ("Username not found.");
            }else{
                user = users.get(0);
            }
        }
        String pwd = user.getPassword();
        if (!password.equals(pwd)) {
            throw new ServletException("Invalid login. Please check your username and password.");
        }
        LoggedInUser logedInUser = new LoggedInUser();
        logedInUser.setId(user.getId());
        logedInUser.setUsername(user.getUsername());
        String token = Jwts.builder().setSubject(username).claim("roles", "user")
                .setIssuedAt(new Date()).signWith(SignatureAlgorithm.HS256, "montrealistoocold").compact();
        logedInUser.setToken(token);
        return logedInUser;
    }
}
