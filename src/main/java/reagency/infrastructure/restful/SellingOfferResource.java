package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.*;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.ApplianceViewModel;
import reagency.infrastructure.restful.viewmodels.EstateViewModel;
import reagency.infrastructure.restful.viewmodels.FacilityViewModel;
import reagency.infrastructure.restful.viewmodels.FeatureViewModel;
import reagency.infrastructure.restful.viewmodels.ImageViewModel;
import reagency.infrastructure.restful.viewmodels.PieceViewModel;
import reagency.infrastructure.restful.viewmodels.SellingOfferExampleView;
import reagency.infrastructure.restful.viewmodels.SellingOfferSearchViewModel;
import reagency.infrastructure.restful.viewmodels.TypeViewModel;
import reagency.infrastructure.restful.viewmodels.UnitViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.estateaggregate.Appliance;
import reagency.core.domain.realestate.estateaggregate.Estate;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;
import reagency.core.domain.realestate.estateaggregate.Image;
import reagency.core.domain.realestate.estateaggregate.NearbyFacility;
import reagency.core.domain.realestate.estateaggregate.Piece;
import reagency.core.domain.realestate.estateaggregate.Unit;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;
import reagency.core.domain.realestate.typeaggregate.EstateFeatureType;
import reagency.core.domain.realestate.typeaggregate.EstateType;
import reagency.core.domain.realestate.typeaggregate.REType;
import reagency.core.domain.realestate.typeaggregate.SellingOfferType;

@RestController
@RequestMapping(value = "/webapi/sellingOffer")
public class SellingOfferResource {

	@RequestMapping(value = "example", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public List<SellingOfferSearchViewModel> findByExample(@RequestBody SellingOfferExampleView exampleView) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		SellingOfferType sellingType = null;
		if (exampleView.getExampleSellingOfferType() != null) {
			sellingType = (SellingOfferType) uow.getrETypeRepository()
					.findById(exampleView.getExampleSellingOfferType().getId());
		}
		List<SellingOffer> sellingOffers = new ArrayList<>();
		if((sellingType != null) || (exampleView.getBasePrice() > 0) || (exampleView.getRegDate() != null)){
			sellingOffers = uow.getSellingOfferRepository()
					.findByPattern(sellingType, exampleView.getBasePrice(),
							exampleView.getExamplePriceCondition(),
							exampleView.getRegDate(),
							exampleView.getExampleRegDateCondition()
							);
		}else{
			sellingOffers = uow.getSellingOfferRepository().findAll();
		}
		
		List<SellingOffer> offers = new ArrayList<>();
		for (SellingOffer sellingOffer : sellingOffers) {
			Estate estate = sellingOffer.getEstate();
			EstateType estateType = null;
			if (exampleView.getExampleEstateType() != null) {
				if (sellingOffer.getEstate().getType() != null) {
					if (estate.getType().getId() != exampleView
							.getExampleEstateType().getId()) {
						break;
					}
				}
			}
			if (exampleView.getAge() != 0) {
				if (exampleView.getExampleAgeCondition() != "") {
					if (exampleView.getExampleAgeCondition() == ">= (Greater Than or Equal)") {
						if (estate.getAge() < exampleView.getAge()) {
							break;
						}
					}
					if (exampleView.getExampleAgeCondition() == "<= (Less Than or Equal)") {
						if (estate.getAge() > exampleView.getAge()) {
							break;
						}
					}
					if (exampleView.getExampleAgeCondition() == "= (Equal)") {
						if (estate.getAge() != exampleView.getAge()) {
							break;
						}
					}
				}
			}
			if(exampleView.getDistrictId() > 0){
				if (exampleView.getDistrictId() != estate.getAreaId()) {
					break;
				}
			}
			offers.add(sellingOffer);
		}
		List<SellingOffer> offersExampleEstateLevel = new ArrayList<>();
		if(offers.size() > 0){
			for (SellingOffer sellingOffer : offers) {
				Estate estate = sellingOffer.getEstate();
				Map<String, Integer> estateFacilitiesMap = new HashMap<String,Integer>();
				estate.getNearbyFacilities().size();
				List<NearbyFacility> facilities = estate.getNearbyFacilities();
				if(exampleView.getFacilitiesExample().size() > 0){
					if(!hasFacilities(facilities, exampleView.getFacilitiesExample())){
						break;
					}
				}
				
				estate.getNearbyFacilities().size();
				List<EstateFeature> features = estate.getFeatures();
				if(exampleView.getFeaturesExample().size() > 0){
					if(!hasFeatures(features, exampleView.getFeaturesExample())){
						break;
					}
				}
				
				offersExampleEstateLevel.add(sellingOffer);
			}
		}
		List<SellingOffer> finalResult = new ArrayList<>();
		if(offersExampleEstateLevel.size() > 0){
			for (SellingOffer sellingOffer : offers) {
				Estate estate = sellingOffer.getEstate();
				estate.getUnits().size();
				if(estate.getUnits().size() >= 0){
					List<Unit> units = estate.getUnits();
					int counter = 0;
					for(Unit unit: units){
						unit.getPieces().size();
						List<Piece> pieces = unit.getPieces();
						for (Piece piece : pieces) {
							counter += piece.getQuantity();
						}
					}
					if(counter >= exampleView.getPieceNum()){
						finalResult.add(sellingOffer);
					}
				}else{
					if(exampleView.getPieceNum() > 0 && estate.getUnits().size() < 0){
					 break;
					}
				}
			}
		}
		
		List<SellingOfferSearchViewModel> allsellingOffers = new ArrayList<>();
		for (SellingOffer offer : finalResult) {
			SellingOfferSearchViewModel sellModel = new SellingOfferSearchViewModel();
			sellModel.setSellingOfferId(offer.getId());
			sellModel.setIsSold(offer.getIsSold());
			if (offer.getType() != null) {
				sellModel.setSellingOfferType(new TypeViewModel(offer.getType()
						.getId(), offer.getType().getName()));
			}
			if (offer.getEstate() != null) {
				if (offer.getEstate().getType() != null) {
					sellModel.setEstateType(new TypeViewModel(offer.getEstate()
							.getType().getId(), offer.getEstate().getType()
							.getName()));
				}

				sellModel.setAge(offer.getEstate().getAge());
				if (offer.getEstate().getFeatures() != null) {
					if (offer.getEstate().getFeatures().size() > 0) {
						sellModel.setHasFeature(true);
					}
				} else {
					sellModel.setHasFeature(false);
				}
				if (offer.getEstate().getUnits() != null) {
					if (offer.getEstate().getUnits().size() > 0) {
						sellModel.setHasUnit(true);
						int pieceNum = 0;
						List<Unit> units = offer.getEstate().getUnits();
						for (Unit unit : units) {
							List<Piece> pieces = unit.getPieces();
							if (unit.getPieces() != null) {
								if (pieces.size() > 0) {
									for (Piece piece : pieces) {
										pieceNum += piece.getQuantity();
									}
								}
							}
						}
						sellModel.setPieceNumbers(pieceNum);
					}
				} else {
					sellModel.setHasUnit(false);
					sellModel.setPieceNumbers(0);
				}

				sellModel.setHasImage(false);
				if (offer.getEstate().getImages() != null) {
					if (offer.getEstate().getImages().size() > 0) {
						sellModel.setHasImage(true);
						String imageData = offer.getEstate().getImages().get(0)
								.getImageData();
						sellModel.setImageData(imageData);

					} else {
						if (offer.getEstate().getUnits() != null) {
							if (offer.getEstate().getUnits().size() > 0) {
								List<Unit> units = offer.getEstate().getUnits();
								for (Unit unit : units) {
									if (unit.getImages() != null) {
										if (unit.getImages().size() > 0) {
											sellModel.setHasImage(true);
											String imageData = unit.getImages()
													.get(0).getImageData();
											sellModel.setImageData(imageData);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			sellModel.setPrice(offer.getBasePrice());
			if(offer.getIsSold() != false){
				sellModel.setIsSold(true);
			}else{
				sellModel.setIsSold(false);
			}
			
			allsellingOffers.add(sellModel);
		}
		uow.commit();
		context.close();
		System.out.println(allsellingOffers.size());
		return allsellingOffers;
	}
	public static boolean hasFacilities(List<NearbyFacility> facilities, List<TypeViewModel> models){
		Map<String, Integer> estateFacilitiesMap = new HashMap<String,Integer>();
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<REType> lst = uow.getrETypeRepository().getTypes("FacilityType");
		for (REType reType : lst) {
			estateFacilitiesMap.put(reType.getName(), -1);
		}
		for (NearbyFacility nearbyFacility : facilities) {
			//int count  = estateFacilitiesMap.get(nearbyFacility.getType().getName());
			estateFacilitiesMap.put(nearbyFacility.getType().getName(), 1);
		}
		for (TypeViewModel typeViewModel : models) {
			if(estateFacilitiesMap.get(typeViewModel.getType()) < 0){
				return false;
			}
		}
		return true;
	}
	public static boolean hasFeatures(List<EstateFeature> features, List<TypeViewModel> models){
		Map<String, Integer> estateFeaturesMap = new HashMap<String,Integer>();
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<REType> lst = uow.getrETypeRepository().getTypes("EstateFeatureType");
		for (REType reType : lst) {
			estateFeaturesMap.put(reType.getName(), -1);
		}
		for (EstateFeature feature : features) {
			//int count  = estateFeaturesMap.get(feature.getFeatureType().getName());
			estateFeaturesMap.put(feature.getFeatureType().getName(), 1);
		}
		for (TypeViewModel typeViewModel : models) {
			if(estateFeaturesMap.get(typeViewModel.getType()) < 0){
				return false;
			}
		}
		return true;
	}

	@RequestMapping(value = "all", method = RequestMethod.GET, produces = "application/json")
	public List<SellingOfferSearchViewModel> getAllSellingOffer() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<SellingOffer> offers = uow.getSellingOfferRepository().findAll();

		List<SellingOfferSearchViewModel> allsellingOffers = new ArrayList<>();
		for (SellingOffer offer : offers) {
			SellingOfferSearchViewModel sellModel = new SellingOfferSearchViewModel();
			sellModel.setSellingOfferId(offer.getId());
			sellModel.setIsSold(offer.getIsSold());
			if (offer.getType() != null) {
				sellModel.setSellingOfferType(new TypeViewModel(offer.getType()
						.getId(), offer.getType().getName()));
			}
			if (offer.getEstate() != null) {
				if (offer.getEstate().getType() != null) {
					sellModel.setEstateType(new TypeViewModel(offer.getEstate()
							.getType().getId(), offer.getEstate().getType()
							.getName()));
				}

				sellModel.setAge(offer.getEstate().getAge());
				if (offer.getEstate().getFeatures() != null) {
					if (offer.getEstate().getFeatures().size() > 0) {
						sellModel.setHasFeature(true);
					}
				} else {
					sellModel.setHasFeature(false);
				}
				if (offer.getEstate().getUnits() != null) {
					if (offer.getEstate().getUnits().size() > 0) {
						sellModel.setHasUnit(true);
						int pieceNum = 0;
						List<Unit> units = offer.getEstate().getUnits();
						for (Unit unit : units) {
							List<Piece> pieces = unit.getPieces();
							if (unit.getPieces() != null) {
								if (pieces.size() > 0) {
									for (Piece piece : pieces) {
										pieceNum += piece.getQuantity();
									}
								}
							}
						}
						sellModel.setPieceNumbers(pieceNum);
					}
				} else {
					sellModel.setHasUnit(false);
					sellModel.setPieceNumbers(0);
				}

				sellModel.setHasImage(false);
				if (offer.getEstate().getImages() != null) {
					if (offer.getEstate().getImages().size() > 0) {
						sellModel.setHasImage(true);
						String imageData = offer.getEstate().getImages().get(0)
								.getImageData();
						sellModel.setImageData(imageData);

					} else {
						if (offer.getEstate().getUnits() != null) {
							if (offer.getEstate().getUnits().size() > 0) {
								List<Unit> units = offer.getEstate().getUnits();
								for (Unit unit : units) {
									if (unit.getImages() != null) {
										if (unit.getImages().size() > 0) {
											sellModel.setHasImage(true);
											String imageData = unit.getImages()
													.get(0).getImageData();
											sellModel.setImageData(imageData);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
			sellModel.setPrice(offer.getBasePrice());
			if(offer.getIsSold() != false){
				sellModel.setIsSold(true);
			}else{
				sellModel.setIsSold(false);
			}
			
			allsellingOffers.add(sellModel);
		}

		uow.commit();
		context.close();
		return allsellingOffers;
	}

	@RequestMapping(value = "details/{sellingOfferId}", method = RequestMethod.GET, produces = "application/json")
	public EstateViewModel getSellingOfferDetails(
			@PathVariable("sellingOfferId") int sellingOfferId) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		SellingOffer offer = uow.getSellingOfferRepository().findById(
				sellingOfferId);
		// List<SellingOfferviewModel> sellingOfferView = new ArrayList<>();

		EstateViewModel estatemodel = new EstateViewModel();
		// /estate part:

		if (offer.getEstate() != null) {
			Estate estate = offer.getEstate();
			estatemodel.setAge(estate.getAge());
			estatemodel.setBasePrice(offer.getBasePrice());
			estatemodel.setDistrictId(estate.getAreaId());
			if (estate.getType() != null) {
				estatemodel.setEstateType(new TypeViewModel(estate.getType()
						.getId(), estate.getType().getName()));
			}
			if (offer.getEstate().getAddress() != null) {
				estatemodel.setNumber(offer.getEstate().getAddress()
						.getNumber());
				estatemodel.setPostalcode(offer.getEstate().getAddress()
						.getPostalCode());
				estatemodel.setStreet(estate.getAddress().getStreet());
			}
			if (estate.getRenewDate() != null) {
				String date = estate.getRenewDate().toString();
				estatemodel.setRenewDate(date.split(" ")[0]);
			}

			estatemodel.setSellingExtraInfo(offer.getDescription());
			if (offer.getType() != null) {
				estatemodel.setSellingOfferType(new TypeViewModel(offer
						.getType().getId(), offer.getType().getName()));
			}
			
			estatemodel.setDiscription(estate.getDescription());
			List<ImageViewModel> estateImages = new ArrayList<>();
			int index = 0;
			for (Image estateImage : estate.getImages()) {
				String img = estateImage.getImageData();
				TypeViewModel typemodel = new TypeViewModel(estateImage
						.getType().getId(), estateImage.getType().getName());
				estateImages.add(new ImageViewModel(index, typemodel, img));
				index++;
			}
			estatemodel.setImages(estateImages);
			// // facilities
			List<FacilityViewModel> estateFacilities = new ArrayList<>();
			index = 0;
			for (NearbyFacility facility : estate.getNearbyFacilities()) {
				FacilityViewModel facilityModel = new FacilityViewModel();
				if (facility.getType() != null) {
					TypeViewModel typemodel = new TypeViewModel(facility
							.getType().getId(), facility.getType().getName());
					facilityModel.setFacilityType(typemodel);
				}
				facilityModel.setIndex(index);
				facilityModel.setUnit(facility.getUnit());
				facilityModel.setValue(facility.getValue() + "");

				estateFacilities.add(facilityModel);
				index++;
			}

			estatemodel.setFacilities(estateFacilities);
			// //////feature part
			List<FeatureViewModel> estateFeatures = new ArrayList<>();
			index = 0;
			for (EstateFeature featuer : estate.getFeatures()) {
				FeatureViewModel featureModel = new FeatureViewModel();
				if (featuer.getFeatureType() != null) {
					TypeViewModel typemodel = new TypeViewModel(featuer
							.getFeatureType().getId(), featuer.getFeatureType()
							.getName());
					featureModel.setFeatureType(typemodel);
				}
				featureModel.setIndex(index);
				featureModel.setQuantity(featuer.getQuantity() + "");
				estateFeatures.add(featureModel);
				index++;
			}

			estatemodel.setFeatures(estateFeatures);

			// //unit part
			List<UnitViewModel> estateUnits = new ArrayList<>();
			index = 0;
			for (Unit unit : estate.getUnits()) {
				UnitViewModel unitViewModel = new UnitViewModel();
				unitViewModel.setFloornumber(unit.getFloorNumber() + "");
				unitViewModel.setIndex(index);
				unitViewModel.setUnitExtraInfo(unit.getDescription());
				unitViewModel.setUnitnumber(unit.getUnitNumber() + "");
				if (unit.getInternalRenewDate() != null) {
					String date = unit.getInternalRenewDate().toString();
					unitViewModel.setUnitRenewDate(date.split(" ")[0]);
				}

				// // Applianace part
				List<ApplianceViewModel> appliances = new ArrayList<>();
				int appIndex = 0;
				for (Appliance appliance : unit.getAppliances()) {
					ApplianceViewModel applianceViewModel = new ApplianceViewModel();
					if (appliance.getType() != null) {

						TypeViewModel typemodel = new TypeViewModel(appliance
								.getType().getId(), appliance.getType()
								.getName());
						applianceViewModel.setApplianceType(typemodel);
					}
					applianceViewModel.setIndex(appIndex);
					applianceViewModel.setApplianceQ(appliance.getQuantity()
							+ "");
					applianceViewModel.setApplianceExtraInfo(appliance
							.getDescription());

					appliances.add(applianceViewModel);
					appIndex++;
				}

				unitViewModel.setAppliances(appliances);
				// // Pieces part
				List<PieceViewModel> pieces = new ArrayList<>();
				int pieceIndex = 0;
				for (Piece piece : unit.getPieces()) {
					PieceViewModel pieceViewModel = new PieceViewModel();
					if (piece.getType() != null) {
						TypeViewModel typemodel = new TypeViewModel(piece
								.getType().getId(), piece.getType().getName());
						pieceViewModel.setPieceType(typemodel);
					}
					pieceViewModel.setIndex(pieceIndex);
					pieceViewModel.setPieceQ(piece.getQuantity() + "");
					pieceViewModel.setPieceExtraInfo(piece.getDescription());

					pieces.add(pieceViewModel);
					pieceIndex++;
				}

				unitViewModel.setPieces(pieces);

				List<ImageViewModel> unitImages = new ArrayList<>();
				int unitImageIndex = 0;
				for (Image unitImage : unit.getImages()) {
					String img = unitImage.getImageData();
					if (unitImage.getType() != null) {
						TypeViewModel typemodel = new TypeViewModel(unitImage
								.getType().getId(), unitImage.getType()
								.getName());
						unitImages.add(new ImageViewModel(unitImageIndex,
								typemodel, img));
						unitImageIndex++;
					}

				}
				unitViewModel.setUnitImages(unitImages);

				estateUnits.add(unitViewModel);
				index++;
			}
			estatemodel.setUnits(estateUnits);
		}

		uow.commit();
		context.close();
		return estatemodel;
	}

}
