package reagency.infrastructure.restful;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.ApplianceViewModel;
import reagency.infrastructure.restful.viewmodels.FacilityViewModel;
import reagency.infrastructure.restful.viewmodels.FeatureViewModel;
import reagency.infrastructure.restful.viewmodels.ImageViewModel;
import reagency.infrastructure.restful.viewmodels.PieceViewModel;
import reagency.infrastructure.restful.viewmodels.UnitViewModel;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mysql.jdbc.Blob;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.estateaggregate.Appliance;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;
import reagency.core.domain.realestate.estateaggregate.Image;
import reagency.core.domain.realestate.estateaggregate.NearbyFacility;
import reagency.core.domain.realestate.estateaggregate.Piece;
import reagency.core.domain.realestate.estateaggregate.Unit;
import reagency.core.domain.realestate.typeaggregate.ApplianceType;
import reagency.core.domain.realestate.typeaggregate.EstateFeatureType;
import reagency.core.domain.realestate.typeaggregate.FacilityType;
import reagency.core.domain.realestate.typeaggregate.ImageType;
import reagency.core.domain.realestate.typeaggregate.PieceType;
import reagency.core.domain.realestate.typeaggregate.REType;

public class Utility {
	public static Date getDate(String dateString){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		try {
			date = formatter.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}
	
	public static List<Appliance> getAppliances(List<ApplianceViewModel> appModels){
		List<Appliance> apps = new ArrayList<>();
		for (ApplianceViewModel view : appModels) {
			Appliance app = new Appliance();
			app.setDescription(view.getApplianceExtraInfo());
			app.setQuantity(Integer.parseInt(view.getApplianceQ()));
			
			app.setType((ApplianceType)getREType(view.getApplianceType().getId()));
			apps.add(app);
		}
		return apps;
	}
	
	public static List<Piece> getPieces(List<PieceViewModel> appModels){
		List<Piece> apps = new ArrayList<>();
		for (PieceViewModel view : appModels) {
			Piece app = new Piece();
			app.setDescription(view.getPieceExtraInfo());
			app.setQuantity(Integer.parseInt(view.getPieceQ()));
			app.setType((PieceType)getREType(view.getPieceType().getId()));
			apps.add(app);
		}
		return apps;
	}
	public static List<NearbyFacility> getFacilitis(List<FacilityViewModel> appModels){
		List<NearbyFacility> apps = new ArrayList<>();
		for (FacilityViewModel view : appModels) {
			NearbyFacility app = new NearbyFacility();
			app.setUnit(view.getUnit());
			app.setValue(Integer.parseInt(view.getValue()));
			app.setType((FacilityType)getREType(view.getFacilityType().getId()));
			apps.add(app);
		}
		return apps;
	}
	public static List<EstateFeature> getFeatures(List<FeatureViewModel> appModels){
		List<EstateFeature> apps = new ArrayList<>();
		for (FeatureViewModel view : appModels) {
			EstateFeature app = new EstateFeature();
			app.setQuantity(Integer.parseInt(view.getQuantity()));
			app.setFeatureType((EstateFeatureType)getREType(view.getFeatureType().getId()));
			apps.add(app);
		}
		return apps;
	}
	public static List<Image> getImages(List<ImageViewModel> appModels){
		List<Image> images = new ArrayList<>();
		for (ImageViewModel view : appModels) {
			Image app = new Image();
			app.setImageData(view.getImage());
			app.setType((ImageType)getREType(view.getType().getId()));
			images.add(app);
		}
		return images;
	}
	private static REType getREType(int id){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		REType type = uow.getrETypeRepository().findById(id);
		uow.commit();
		context.close();
		return type;
	}
	//public static getDistrictById(int id){
//		Class.forName("com.mysql.jdbc.Driver");
//		java.sql.Connection conn = DriverManager.getConnection(
//				"jdbc:mysql://localhost:3306/reagency", "root", "123456");
//
//		Statement stmt = conn.createStatement();
//		String sql;
//		sql = "SELECT * FROM reagency.district where ID = " + id;
//		ResultSet rs = stmt.executeQuery(sql);
//		rs.get

//	}
	
	

}
