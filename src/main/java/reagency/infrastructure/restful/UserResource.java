package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.*;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.TypeViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IRETypeRepository;
import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.contract.infracontract.IUserRepository;
import reagency.core.domain.kernel.User;
import reagency.core.domain.realestate.typeaggregate.REType;
import reagency.core.domainservice.imp.RETypeService;

@RestController
@RequestMapping(value = "/webapi/user")
public class UserResource {

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<User> getAllUser(){
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		
		List<User> users = uow.getUserRepository().findAll();
		uow.commit();
		context.close();
//		List<TypeViewModel> result = new ArrayList<>();
//		for(REType type: types){
//			TypeViewModel model = new TypeViewModel();
//			model.setType(type.getName());
//			model.setId(type.getId());
//			result.add(model);
//		}
//		
		return users;
	}

	@RequestMapping(value = "delete/{userId}", method = RequestMethod.GET, produces = "application/json")
	public String deleteType(@PathVariable("userId") int userId){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		
		boolean isDone = uow.getUserRepository().delete(userId);
		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}

	@RequestMapping(value = "update/user", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String updateType(@RequestBody User user){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		IUserRepository repo = uow.getUserRepository();
		User original = repo.findById(user.getId());
		if( !original.getName().equalsIgnoreCase(user.getName())){
			original.setName(user.getName());
		}
//		if( !original.getName().equalsIgnoreCase(user.getName())){
//			original.setName(user.getName());
//		}if( !original.getName().equalsIgnoreCase(user.getName())){
//			original.setName(user.getName());
//		}if( !original.getName().equalsIgnoreCase(user.getName())){
//			original.setName(user.getName());
//		}if( !original.getName().equalsIgnoreCase(user.getName())){
//			original.setName(user.getName());
//		}
//		
		
		boolean isDone = true;
		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());//taghir konad ba isDone
	}

}
