package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.HibernateUtility;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.LocationViewModel;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.hibernate.Session;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.mysql.jdbc.Connection;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.Country;
import reagency.core.domain.realestate.areaaggregate.District;
import reagency.core.domain.realestate.areaaggregate.State;

@RestController
@RequestMapping(value = "/webapi/location")
public class LocationResource {

	@RequestMapping(value = "countries", method = RequestMethod.GET, produces = "application/json")
	public List<LocationViewModel> getCountries() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		List<Country> countries = uow.getCountryRepository().findAll();
		List<LocationViewModel> result = new ArrayList<>();
		for (Country country : countries) {
			LocationViewModel c = new LocationViewModel();
			c.setName(country.getName());
			c.setId(country.getId());
			result.add(c);
		}
		uow.commit();
		context.close();
		return result;
	}

	@RequestMapping(value = "state/{countryId}", method = RequestMethod.GET, produces = "application/json")
	public List<LocationViewModel> getStates(
			@PathVariable("countryId") int countryId) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		Country country = uow.getCountryRepository().findById(countryId);
		List<LocationViewModel> result = new ArrayList<>();
		if (country != null) {
			int size = country.getStates().size();
			List<State> states = country.getStates();
			for (State state : states) {
				LocationViewModel c = new LocationViewModel();
				c.setName(state.getName());
				c.setId(state.getId());
				result.add(c);
			}
		}
		uow.commit();
		context.close();
		return result;
	}

	@RequestMapping(value = "city/{stateId}", method = RequestMethod.GET, produces = "application/json")
	public List<LocationViewModel> getCities(@PathVariable("stateId") int stateId) {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		State state = uow.getStateRepository().findById(stateId);
		List<State> states = uow.getStateRepository().findAll();
		List<City> cit = uow.getCityRepository().findAll();
		List<City> cities = new ArrayList<>();
		for (City city : cit) {
			if (city.getState().getId() == stateId)
				cities.add(city);
		}

		List<LocationViewModel> result = new ArrayList<>();
		for (City city : cities) {
			LocationViewModel c = new LocationViewModel();
			c.setName(city.getName());
			c.setId(city.getId());
			result.add(c);
		}
		uow.commit();
		context.close();

		return result;
	}

	@RequestMapping(value = "district/{cityId}", method = RequestMethod.GET, produces = "application/json")
	public List<LocationViewModel> getDistrics(@PathVariable("cityId") int cityId)
			throws ClassNotFoundException, SQLException {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		// State state = uow.getStateRepository().findById(1);
		// List<District> dists = uow.getDistrictRepository().findAll();
		// List<District> districts = new ArrayList<>();
		// for(District district : dists){
		// if(district.getCity().getId() == cityId)
		// districts.add(district);
		// }
		Class.forName("com.mysql.jdbc.Driver");
		java.sql.Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/reagency", "root", "123456");

		Statement stmt = conn.createStatement();
		String sql;
		sql = "SELECT * FROM reagency.district where city = " + cityId;
		ResultSet rs = stmt.executeQuery(sql);

		// STEP 5: Extract data from result set
		List<LocationViewModel> result = new ArrayList<>();
		while (rs.next()) {
			LocationViewModel c = new LocationViewModel();
			c.setName(rs.getString("NAME"));
			c.setId(rs.getInt("ID"));
			result.add(c);
		}
		uow.commit();
		context.close();

		return result;
	}

}
