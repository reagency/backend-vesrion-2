package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.MailSenderViewModel;
import reagency.infrastructure.restful.viewmodels.ServerInfoViewModel;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.ServerInfo;
@RestController
@RequestMapping(value = "/webapi/server")
public class ServerResource {
	@RequestMapping(value = "sender", method = RequestMethod.GET, produces = "application/json")
	public MailSenderViewModel getMailSender() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo sender = null;
		if (senders.size() > 0) {
			sender = senders.get(0);
		}
		MailSenderViewModel model = new MailSenderViewModel();
		if (sender != null) {
			model.setEmail(sender.getEmail());
			model.setPassword(sender.getPassword());
		}
		uow.commit();
		context.close();
		return model;
	}

	@RequestMapping(value = "update/sender", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String updateSender(@RequestBody MailSenderViewModel sender) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo senderObj = null;
		if (senders.size() > 0) {
			senderObj = senders.get(0);
		}
		boolean isDone = false;
		if (senderObj != null) {

			if (!senderObj.getEmail().equalsIgnoreCase(sender.getEmail())) {
				senderObj.setEmail(sender.getEmail());
			}
			if (!senderObj.getPassword().equalsIgnoreCase(sender.getPassword())) {
				senderObj.setPassword(sender.getPassword());
			}
			uow.getServerInfoRepository().update(senderObj);
			isDone = true;
		} else {
			ServerInfo newSender = new ServerInfo();
			newSender.setEmail(sender.getEmail());
			newSender.setPassword(sender.getPassword());
			newSender.setIsArchived(false);
			uow.getServerInfoRepository().save(newSender);
			isDone = true;
		}

		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}

	@RequestMapping(value = "/serverInfo", method = RequestMethod.GET, produces = "application/json")
	public ServerInfoViewModel getServerInfo() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo sender = null;
		if (senders.size() > 0) {
			sender = senders.get(0);
		}
		ServerInfoViewModel model = new ServerInfoViewModel();
		if (sender != null) {
			model.setOfferExpiry(sender.getOfferExpiry());
			model.setSellingExpiry(sender.getSellingExpiry());
		}
		uow.commit();
		context.close();
		return model;
	}

	@RequestMapping(value = "update/offersetting", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String updateOffersSetting(@RequestBody ServerInfoViewModel info) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<ServerInfo> servers = uow.getServerInfoRepository().findAll();
		ServerInfo server = null;
		if(servers.size() > 0){
			server = servers.get(0);
		}
		
		boolean isDone = false;
		if (server != null) {

			if (server.getOfferExpiry() != info.getOfferExpiry()) {
				server.setOfferExpiry(info.getOfferExpiry());
			}
			if (server.getSellingExpiry() != info.getSellingExpiry()) {
				server.setSellingExpiry(info.getSellingExpiry());
			}
			uow.getServerInfoRepository().update(server);
			isDone = true;
		} else {
			ServerInfo newServer = new ServerInfo();
			newServer.setOfferExpiry(info.getOfferExpiry());
			newServer.setSellingExpiry(info.getSellingExpiry());
			uow.getServerInfoRepository().save(newServer);
			isDone = true;
		}

		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}


}
