package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.*;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.TypeViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.realestate.typeaggregate.REType;
import reagency.core.domainservice.imp.RETypeService;

@RestController
@RequestMapping(value = "/webapi/type")
public class TypeResource {

	@RequestMapping(value = "{typeName}", method = RequestMethod.GET, produces = "application/json")
	public List<TypeViewModel> getTypeByName(@PathVariable("typeName") String typeName){
		
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		
		List<REType> types = uow.getrETypeRepository().getTypesOrderByRank(typeName);
		List<TypeViewModel> result = new ArrayList<>();
		for(REType type : types){
			TypeViewModel t = new TypeViewModel();
			t.setId(type.getId());
			t.setType(type.getName());
			result.add(t);
		}
		uow.commit();
		context.close();
		return result;
	}

	@RequestMapping(value = "delete/{typeId}", method = RequestMethod.GET, produces = "application/json")
	public String deleteType(@PathVariable("typeId") int typeId){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		
		boolean isDone = uow.getrETypeRepository().delete(typeId);
		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}

	@RequestMapping(value = "{typeName}/newtype", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addNewType(@PathVariable("typeName") String typeName, @RequestBody TypeViewModel newType){
		boolean result = false;
		RETypeService service = new RETypeService();
		if(newType != null){
			result = service.AddNewTypeToOneREType(newType.getType(), typeName);
		}
		return (new Boolean(result).toString());
	}

	@RequestMapping(value = "update/type", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String updateType(@RequestBody TypeViewModel type){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		REType before = uow.getrETypeRepository().findById(type.getId());
		if(before != null){
			if( !before.getName().equalsIgnoreCase(type.getType())){
				before.setName(type.getType());
			}
		}
		
		boolean isDone = uow.getrETypeRepository().update(before);
		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}
	
}
