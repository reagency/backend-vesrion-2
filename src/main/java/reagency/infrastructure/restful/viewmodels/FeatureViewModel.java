package reagency.infrastructure.restful.viewmodels;

public class FeatureViewModel {
	private int index;
	private String quantity;
	private TypeViewModel featureType;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public TypeViewModel getFeatureType() {
		return featureType;
	}
	public void setFeatureType(TypeViewModel featureType) {
		this.featureType = featureType;
	}
	

}
