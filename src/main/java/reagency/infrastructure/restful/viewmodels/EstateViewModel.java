package reagency.infrastructure.restful.viewmodels;

import java.util.ArrayList;
import java.util.List;

public class EstateViewModel{
	private int countryId;
	private int stateId;
	private int cityId;
	private int districtId;
	private String street;
	private String number;
	private String postalcode;
	private TypeViewModel estateType;
	private String renewDate;
	private int age;
	private List<FeatureViewModel> features = new ArrayList<>();
	private List<FacilityViewModel> facilities = new ArrayList<>();
	private List<ImageViewModel> images = new ArrayList<>();
	private List<UnitViewModel> units = new ArrayList<>();
	private int basePrice;
    private TypeViewModel sellingOfferType;
    private String sellingExtraInfo;

	
	private String discription;
	
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public TypeViewModel getEstateType() {
		return estateType;
	}
	public void setEstateType(TypeViewModel estateType) {
		this.estateType = estateType;
	}
	public String getRenewDate() {
		return renewDate;
	}
	public void setRenewDate(String renewDate) {
		this.renewDate = renewDate;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
//	public List<FeatureViewModel> getFeatures() {
//		return features;
//	}
//	public void setFeatures(List<FeatureViewModel> features) {
//		this.features = features;
//	}
	public List<FeatureViewModel> getFeatures() {
		return features;
	}
	public void setFeatures(List<FeatureViewModel> features) {
		this.features = features;
	}
	public List<FacilityViewModel> getFacilities() {
		return facilities;
	}
	public void setFacilities(List<FacilityViewModel> facilities) {
		this.facilities = facilities;
	}
	public List<ImageViewModel> getImages() {
		return images;
	}
	public void setImages(List<ImageViewModel> images) {
		this.images = images;
	}
	public List<UnitViewModel> getUnits() {
		return units;
	}
	public void setUnits(List<UnitViewModel> units) {
		this.units = units;
	}
	public int getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(int basePrice) {
		this.basePrice = basePrice;
	}
	public TypeViewModel getSellingOfferType() {
		return sellingOfferType;
	}
	public void setSellingOfferType(TypeViewModel sellingOfferType) {
		this.sellingOfferType = sellingOfferType;
	}
	public String getSellingExtraInfo() {
		return sellingExtraInfo;
	}
	public void setSellingExtraInfo(String sellingExtraInfo) {
		this.sellingExtraInfo = sellingExtraInfo;
	}
	
	
}
