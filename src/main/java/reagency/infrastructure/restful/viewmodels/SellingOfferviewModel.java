package reagency.infrastructure.restful.viewmodels;


public class SellingOfferviewModel {
	private int id;
	private EstateViewModel estate;
	private TypeViewModel type;
	private String description;
	private int basePrice;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EstateViewModel getEstate() {
		return estate;
	}

	public void setEstate(EstateViewModel estate) {
		this.estate = estate;
	}

	public TypeViewModel getType() {
		return type;
	}

	public void setType(TypeViewModel type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(int basePrice) {
		this.basePrice = basePrice;
	}

}
