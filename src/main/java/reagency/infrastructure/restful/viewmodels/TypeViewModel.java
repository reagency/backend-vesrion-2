package reagency.infrastructure.restful.viewmodels;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TypeViewModel {
	public TypeViewModel() {
		// TODO Auto-generated constructor stub
	}
	
	public TypeViewModel(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	private int id;
	private String type;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
