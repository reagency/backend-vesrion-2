package reagency.infrastructure.restful.viewmodels;

public class SellingOfferSearchViewModel {
	private int sellingOfferId;
	private TypeViewModel estateType;
	private TypeViewModel sellingOfferType;
	private int price;
	private int pieceNumbers;
	private boolean hasFeature;
	private boolean hasUnit;
	private boolean hasImage;
	private String imageData;
	private int age;
	private boolean isSold;
	
	
	public TypeViewModel getEstateType() {
		return estateType;
	}
	public void setEstateType(TypeViewModel estateType) {
		this.estateType = estateType;
	}
	public TypeViewModel getSellingOfferType() {
		return sellingOfferType;
	}
	public void setSellingOfferType(TypeViewModel sellingOfferType) {
		this.sellingOfferType = sellingOfferType;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getPieceNumbers() {
		return pieceNumbers;
	}
	public void setPieceNumbers(int pieceNumbers) {
		this.pieceNumbers = pieceNumbers;
	}
	public boolean isHasFeature() {
		return hasFeature;
	}
	public void setHasFeature(boolean hasFeature) {
		this.hasFeature = hasFeature;
	}
	public boolean isHasUnit() {
		return hasUnit;
	}
	public void setHasUnit(boolean hasUnit) {
		this.hasUnit = hasUnit;
	}
	public boolean isHasImage() {
		return hasImage;
	}
	public void setHasImage(boolean hasImage) {
		this.hasImage = hasImage;
	}
	public String getImageData() {
		return imageData;
	}
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSellingOfferId() {
		return sellingOfferId;
	}
	public void setSellingOfferId(int sellingOfferId) {
		this.sellingOfferId = sellingOfferId;
	}
	public boolean getIsSold() {
		return isSold;
	}
	public void setIsSold(boolean isSold) {
		this.isSold = isSold;
	}
	
}
