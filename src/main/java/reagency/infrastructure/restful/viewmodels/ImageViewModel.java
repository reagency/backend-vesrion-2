package reagency.infrastructure.restful.viewmodels;

public class ImageViewModel {
	public ImageViewModel() {
		// TODO Auto-generated constructor stub
	}
	
	public ImageViewModel(int index, TypeViewModel type, String image) {
		super();
		this.index = index;
		this.type = type;
		this.image = image;
	}

	private int index;
	private TypeViewModel type;
	private String image;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public TypeViewModel getType() {
		return type;
	}
	public void setType(TypeViewModel type) {
		this.type = type;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	

}
