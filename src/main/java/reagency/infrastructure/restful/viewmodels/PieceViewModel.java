package reagency.infrastructure.restful.viewmodels;

public class PieceViewModel {
	private int index;
	private String pieceQ;
	private TypeViewModel pieceType;
	private String pieceExtraInfo;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getPieceQ() {
		return pieceQ;
	}
	public void setPieceQ(String pieceQ) {
		this.pieceQ = pieceQ;
	}
	public TypeViewModel getPieceType() {
		return pieceType;
	}
	public void setPieceType(TypeViewModel pieceType) {
		this.pieceType = pieceType;
	}
	public String getPieceExtraInfo() {
		return pieceExtraInfo;
	}
	public void setPieceExtraInfo(String pieceExtraInfo) {
		this.pieceExtraInfo = pieceExtraInfo;
	}
	

}
