package reagency.infrastructure.restful.viewmodels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SellingOfferExampleView {
	private int districtId;
	private TypeViewModel exampleEstateType;
	private TypeViewModel exampleSellingOfferType;
	private String examplePriceCondition;
	private String exampleAgeCondition;
	private List<TypeViewModel> featuresExample = new ArrayList<>();
	private List<TypeViewModel> facilitiesExample = new ArrayList<>();
	private int pieceNum;
	private int age;
	private int basePrice;
	private Date regDate;
	private String exampleRegDateCondition;
	
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public TypeViewModel getExampleEstateType() {
		return exampleEstateType;
	}
	public void setExampleEstateType(TypeViewModel exampleEstateType) {
		this.exampleEstateType = exampleEstateType;
	}
	public TypeViewModel getExampleSellingOfferType() {
		return exampleSellingOfferType;
	}
	public void setExampleSellingOfferType(TypeViewModel exampleSellingOfferType) {
		this.exampleSellingOfferType = exampleSellingOfferType;
	}
	public String getExamplePriceCondition() {
		return examplePriceCondition;
	}
	public void setExamplePriceCondition(String examplePriceCondition) {
		this.examplePriceCondition = examplePriceCondition;
	}
	public String getExampleAgeCondition() {
		return exampleAgeCondition;
	}
	public void setExampleAgeCondition(String exampleAgeCondition) {
		this.exampleAgeCondition = exampleAgeCondition;
	}
	public List<TypeViewModel> getFeaturesExample() {
		return featuresExample;
	}
	public void setFeaturesExample(List<TypeViewModel> featuresExample) {
		this.featuresExample = featuresExample;
	}
	public List<TypeViewModel> getFacilitiesExample() {
		return facilitiesExample;
	}
	public void setFacilitiesExample(List<TypeViewModel> facilitiesExample) {
		this.facilitiesExample = facilitiesExample;
	}
	public int getPieceNum() {
		return pieceNum;
	}
	public void setPieceNum(int pieceNum) {
		this.pieceNum = pieceNum;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(int basePrice) {
		this.basePrice = basePrice;
	}
	
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getExampleRegDateCondition() {
		return exampleRegDateCondition;
	}
	public void setExampleRegDateCondition(String exampleRegDateCondition) {
		this.exampleRegDateCondition = exampleRegDateCondition;
	}
	
}
