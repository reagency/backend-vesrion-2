package reagency.infrastructure.restful.viewmodels;

public class OfferViewModel {
	private int offerId;
	private String name;
	private String family;
	private String email;
	private String phone;
	private int offerPrice;
	private String offerExtraInfo;
	public int getOfferId() {
		return offerId;
	}
	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(int offerPrice) {
		this.offerPrice = offerPrice;
	}
	public String getOfferExtraInfo() {
		return offerExtraInfo;
	}
	public void setOfferExtraInfo(String offerExtraInfo) {
		this.offerExtraInfo = offerExtraInfo;
	}

}
