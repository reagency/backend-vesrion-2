package reagency.infrastructure.restful.viewmodels;

public class FacilityViewModel {

	private int index;
	private TypeViewModel facilityType;
	private String unit;
	private String value;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public TypeViewModel getFacilityType() {
		return facilityType;
	}
	public void setFacilityType(TypeViewModel facilityType) {
		this.facilityType = facilityType;
	}
	

}
