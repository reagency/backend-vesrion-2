package reagency.infrastructure.restful.viewmodels;

public class ServerInfoViewModel {
	private int offerExpiry;
	private int sellingExpiry;
	
	public int getOfferExpiry() {
		return offerExpiry;
	}
	public void setOfferExpiry(int offerExpiry) {
		this.offerExpiry = offerExpiry;
	}
	public int getSellingExpiry() {
		return sellingExpiry;
	}
	public void setSellingExpiry(int sellingExpiry) {
		this.sellingExpiry = sellingExpiry;
	}

}
