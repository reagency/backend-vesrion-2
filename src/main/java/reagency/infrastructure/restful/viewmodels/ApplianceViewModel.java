package reagency.infrastructure.restful.viewmodels;

public class ApplianceViewModel {
	private int index;
	private String applianceQ;
	private TypeViewModel applianceType;
	private String applianceExtraInfo;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getApplianceQ() {
		return applianceQ;
	}
	public void setApplianceQ(String applianceQ) {
		this.applianceQ = applianceQ;
	}
	public TypeViewModel getApplianceType() {
		return applianceType;
	}
	public void setApplianceType(TypeViewModel applianceType) {
		this.applianceType = applianceType;
	}
	public String getApplianceExtraInfo() {
		return applianceExtraInfo;
	}
	public void setApplianceExtraInfo(String applianceExtraInfo) {
		this.applianceExtraInfo = applianceExtraInfo;
	}
	

}
