package reagency.infrastructure.restful.viewmodels;

import java.util.ArrayList;
import java.util.List;

public class UnitViewModel {
	private int index;
	private String floornumber;
	private String unitnumber;
	private String unitRenewDate;
	private String unitExtraInfo;
	private List<ImageViewModel> unitImages = new ArrayList<>();
	private List<PieceViewModel> pieces = new ArrayList<>();
	private List<ApplianceViewModel> appliances = new ArrayList<>();
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getFloornumber() {
		return floornumber;
	}
	public void setFloornumber(String floornumber) {
		this.floornumber = floornumber;
	}
	public String getUnitnumber() {
		return unitnumber;
	}
	public void setUnitnumber(String unitnumber) {
		this.unitnumber = unitnumber;
	}
	public String getUnitRenewDate() {
		return unitRenewDate;
	}
	public void setUnitRenewDate(String unitRenewDate) {
		this.unitRenewDate = unitRenewDate;
	}
	public String getUnitExtraInfo() {
		return unitExtraInfo;
	}
	public void setUnitExtraInfo(String unitExtraInfo) {
		this.unitExtraInfo = unitExtraInfo;
	}
	public List<ImageViewModel> getUnitImages() {
		return unitImages;
	}
	public void setUnitImages(List<ImageViewModel> unitImages) {
		this.unitImages = unitImages;
	}
	public List<PieceViewModel> getPieces() {
		return pieces;
	}
	public void setPieces(List<PieceViewModel> pieces) {
		this.pieces = pieces;
	}
	public List<ApplianceViewModel> getAppliances() {
		return appliances;
	}
	public void setAppliances(List<ApplianceViewModel> appliances) {
		this.appliances = appliances;
	}
	

}
