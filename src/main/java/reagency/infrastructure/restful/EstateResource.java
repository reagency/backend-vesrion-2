package reagency.infrastructure.restful;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;
import reagency.infrastructure.restful.viewmodels.EstateViewModel;
import reagency.infrastructure.restful.viewmodels.FacilityViewModel;
import reagency.infrastructure.restful.viewmodels.TypeViewModel;
import reagency.infrastructure.restful.viewmodels.UnitViewModel;

import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.applicationservice.MailSender;
import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.ServerInfo;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.Country;
import reagency.core.domain.realestate.areaaggregate.District;
import reagency.core.domain.realestate.areaaggregate.State;
import reagency.core.domain.realestate.estateaggregate.Address;
import reagency.core.domain.realestate.estateaggregate.Appliance;
import reagency.core.domain.realestate.estateaggregate.Estate;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;
import reagency.core.domain.realestate.estateaggregate.Image;
import reagency.core.domain.realestate.estateaggregate.NearbyFacility;
import reagency.core.domain.realestate.estateaggregate.Piece;
import reagency.core.domain.realestate.estateaggregate.Unit;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;
import reagency.core.domain.realestate.typeaggregate.EstateType;
import reagency.core.domain.realestate.typeaggregate.SellingOfferType;

@RestController
@RequestMapping(value = "/webapi/estate")
public class EstateResource {

	@RequestMapping(value = "newEstate", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String addNewType(@RequestBody EstateViewModel estate){
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		
		SellingOffer sellingOffer = new SellingOffer();
		
		sellingOffer.setType((SellingOfferType)uow.getrETypeRepository().findById(estate.getSellingOfferType().getId()));
		sellingOffer.setBasePrice(estate.getBasePrice());
		sellingOffer.setDescription(estate.getSellingExtraInfo());
	
		Estate newEstate = new Estate();
		for(UnitViewModel unitView: estate.getUnits()){
			Unit unit = new Unit();
			List<Piece> pieces = Utility.getPieces(unitView.getPieces());
			for (Piece piece : pieces) {
				unit.addPiece(piece);
			}
			List<Appliance> appliances = Utility.getAppliances(unitView.getAppliances());
			for (Appliance appliance : appliances) {
				unit.addAppliance(appliance);
			}
			List<Image> images = Utility.getImages(unitView.getUnitImages());
			for (Image image : images) {
				unit.addImage(image);
			}
			unit.setDescription(unitView.getUnitExtraInfo());
			unit.setFloorNumber(Integer.parseInt(unitView.getFloornumber()));
			unit.setInternalRenewDate(Utility.getDate(unitView.getUnitRenewDate()));
			unit.setUnitNumber(Integer.parseInt(unitView.getUnitnumber()));
			
			newEstate.addUnits(unit);
		}
		Address address = new Address();
		address.setStreet(estate.getStreet());
		address.setPostalCode(estate.getPostalcode());
		address.setNumber(estate.getNumber());
		
		newEstate.setAddress(address);
		newEstate.setAge(estate.getAge());
		District district = uow.getDistrictRepository().findById(estate.getDistrictId());
		newEstate.setAreaId(estate.getDistrictId());
		newEstate.setDescription(estate.getDiscription());
		newEstate.setRenewDate(Utility.getDate(estate.getRenewDate()));
		List<NearbyFacility> facilities = Utility.getFacilitis(estate.getFacilities());
		
		

		
		
		for (NearbyFacility facility : facilities) {
			newEstate.addNearbyFacilitys(facility);
		}
		List<EstateFeature> features = Utility.getFeatures(estate.getFeatures());
		for (EstateFeature feature : features) {
			newEstate.addEstateFeatures(feature);
		}
		if(estate.getImages() != null){
			List<Image> images = Utility.getImages(estate.getImages());
			for (Image image : images) {
				newEstate.addImage(image);
			}
		}
		EstateType estettype = (EstateType)uow.getrETypeRepository().findById(estate.getEstateType().getId());
		newEstate.setType(estettype);
		
		
		uow.getEstateRepository().save(newEstate);
		sellingOffer.setEstate(newEstate);
		sellingOffer.setRegisterDate(new Date());
		uow.getSellingOfferRepository().save(sellingOffer);
		
		MailSender mailSender = new MailSender();
		String message ="new selling offer is added into system, and your track Id is: "+ sellingOffer.getId();
		ServerInfo server =  uow.getServerInfoRepository().findById(1);
		if(server != null){
			String reciverEmail = server.getEmail();
			String subject = "Selling Offer track Id";
			mailSender.sendEmail(message, reciverEmail, subject);
		}
		uow.commit();
		context.close();
		return (sellingOffer.getId() + "");
	}

}
