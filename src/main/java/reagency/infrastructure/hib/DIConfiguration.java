package reagency.infrastructure.hib;

import reagency.infrastructure.hib.repo.imp.ApplianceRepository;
import reagency.infrastructure.hib.repo.imp.AvailableTimeRepository;
import reagency.infrastructure.hib.repo.imp.BookedTimeRepository;
import reagency.infrastructure.hib.repo.imp.CityRepository;
import reagency.infrastructure.hib.repo.imp.CountryRepository;
import reagency.infrastructure.hib.repo.imp.DistrictRepository;
import reagency.infrastructure.hib.repo.imp.EstateFeatureRepository;
import reagency.infrastructure.hib.repo.imp.EstateRepository;
import reagency.infrastructure.hib.repo.imp.ExpertAgencyRepository;
import reagency.infrastructure.hib.repo.imp.ImageRepository;
import reagency.infrastructure.hib.repo.imp.NearbyFacilityRepository;
import reagency.infrastructure.hib.repo.imp.OfferRepository;
import reagency.infrastructure.hib.repo.imp.PieceRepository;
import reagency.infrastructure.hib.repo.imp.RETypeRepository;
import reagency.infrastructure.hib.repo.imp.RepresenterRepository;
import reagency.infrastructure.hib.repo.imp.RoleRepository;
import reagency.infrastructure.hib.repo.imp.SellingOfferRepository;
import reagency.infrastructure.hib.repo.imp.ServerInfoRepository;
import reagency.infrastructure.hib.repo.imp.StateRepository;
import reagency.infrastructure.hib.repo.imp.UnitRepository;
import reagency.infrastructure.hib.repo.imp.UserRepository;

import org.hibernate.criterion.Distinct;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import reagency.core.contract.infracontract.IApplianceRepository;
import reagency.core.contract.infracontract.IAvailableTimeRepository;
import reagency.core.contract.infracontract.IBookedTimeRepository;
import reagency.core.contract.infracontract.ICityRepository;
import reagency.core.contract.infracontract.ICountryRepository;
import reagency.core.contract.infracontract.IDistrictRepository;
import reagency.core.contract.infracontract.IEstateFeatureRepository;
import reagency.core.contract.infracontract.IEstateRepository;
import reagency.core.contract.infracontract.IExpertAgencyRepository;
import reagency.core.contract.infracontract.IImageRepository;
import reagency.core.contract.infracontract.INearbyFacilityRepository;
import reagency.core.contract.infracontract.IOfferRepository;
import reagency.core.contract.infracontract.IPieceRepository;
import reagency.core.contract.infracontract.IRETypeRepository;
import reagency.core.contract.infracontract.IRepresenterRepository;
import reagency.core.contract.infracontract.IRoleRepository;
import reagency.core.contract.infracontract.ISellingOfferRepository;
import reagency.core.contract.infracontract.IServerInfoRepository;
import reagency.core.contract.infracontract.IStateRepository;
import reagency.core.contract.infracontract.IUnitRepository;
import reagency.core.contract.infracontract.IUserRepository;
import reagency.core.domain.kernel.Role;
import reagency.core.domain.kernel.ServerInfo;
import reagency.core.domain.kernel.User;
import reagency.core.domain.realestate.agantaggregate.ExpertAgency;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.Country;
import reagency.core.domain.realestate.areaaggregate.State;
import reagency.core.domain.realestate.estateaggregate.Appliance;
import reagency.core.domain.realestate.estateaggregate.Estate;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;
import reagency.core.domain.realestate.estateaggregate.Image;
import reagency.core.domain.realestate.estateaggregate.NearbyFacility;
import reagency.core.domain.realestate.estateaggregate.Piece;
import reagency.core.domain.realestate.estateaggregate.Unit;
import reagency.core.domain.realestate.offeringaggregate.AvailableTime;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;
import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.Representer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;
import reagency.core.domain.realestate.typeaggregate.REType;

@Configuration
@ComponentScan(value={"reagency.infrastructure.hib"})
public class DIConfiguration {
    
    @Bean
    public ICityRepository getCityRepository(){
    	return new CityRepository(City.class);
    }
    @Bean
    public IServerInfoRepository getServerInfoRepository(){
    	return new ServerInfoRepository(ServerInfo.class);
    }
    @Bean
    public IRoleRepository getRoleRepository() {
		return new RoleRepository(Role.class);
	}
    @Bean
	public IUserRepository getUserRepository() {
		return new UserRepository(User.class);
	}
    @Bean
	public IExpertAgencyRepository getExpertAgencyRepository() {
		return new ExpertAgencyRepository(ExpertAgency.class);
	}
    @Bean
	public ICountryRepository getCountryRepository() {
		return new CountryRepository(Country.class);
	}
    @Bean
	public IDistrictRepository getDistrictRepository() {
		return new DistrictRepository(Distinct.class);
	}
    @Bean
	public IStateRepository getStateRepository() {
		return new StateRepository(State.class);
	}
    @Bean
	public IApplianceRepository getApplianceRepository() {
		return new ApplianceRepository(Appliance.class);
	}
    @Bean
	public IEstateRepository getEstateRepository() {
		return new EstateRepository(Estate.class);
	}
    @Bean
	public IEstateFeatureRepository getEstateFeatureRepository() {
		return new EstateFeatureRepository(EstateFeature.class);
	}
    @Bean
	public IImageRepository getImageRepository() {
		return new ImageRepository(Image.class);
	}
    @Bean
	public INearbyFacilityRepository getNearbyFacilityRepository() {
		return new NearbyFacilityRepository(NearbyFacility.class);
	}
    @Bean
	public IPieceRepository getPieceRepository() {
		return new PieceRepository(Piece.class);
	}
    @Bean
	public IUnitRepository getUnitRepository() {
		return new UnitRepository(Unit.class);
	}
    @Bean
	public IAvailableTimeRepository getAvailableTimeRepository() {
		return new AvailableTimeRepository(AvailableTime.class);
	}
    @Bean
	public IBookedTimeRepository getBookedTimeRepository() {
		return new BookedTimeRepository(BookedTime.class);
	}
    @Bean
	public IOfferRepository getOfferRepository() {
		return new OfferRepository(Offer.class);
	}
    @Bean
	public IRepresenterRepository getRepresenterRepository() {
		return new RepresenterRepository(Representer.class);
	}
    @Bean
	public ISellingOfferRepository getSellingOfferRepository() {
		return new SellingOfferRepository(SellingOffer.class);
	}
    @Bean
	public IRETypeRepository getrETypeRepository() {
		return new RETypeRepository(REType.class);
	}

}
