package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.IAvailableTimeRepository;
import reagency.core.domain.realestate.offeringaggregate.AvailableTime;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;

public class AvailableTimeRepository extends Repository<AvailableTime>
		implements IAvailableTimeRepository {

	public AvailableTimeRepository(Class<?> type) {
		super(type);
	}
	public List<BookedTime> loadRepresenters(AvailableTime availableTime){
		Hibernate.initialize(availableTime.getBookedTimes());
		return availableTime.getBookedTimes();
	}

}
