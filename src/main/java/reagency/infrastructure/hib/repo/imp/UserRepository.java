package reagency.infrastructure.hib.repo.imp;

import org.hibernate.Query;
import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.IUserRepository;
import reagency.core.domain.kernel.User;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;
import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;

public class UserRepository extends Repository<User> implements IUserRepository {

	public UserRepository(Class<?> type) {
		super(type);
	}
	public List<BookedTime> loadBookedTimes(User user){
		Hibernate.initialize(user.getBookedTimes());
		return user.getBookedTimes();
	}
	public List<SellingOffer> loadFavorites(User user){
		Hibernate.initialize(user.getFavorites());
		return user.getFavorites();
	}
	public List<Offer> loadOffers(User user){
		Hibernate.initialize(user.getOffers());
		return user.getOffers();
	}
	public List<BookedTime> loadSellingOffers(User user){
		Hibernate.initialize(user.getSellingOffers());
		return user.getBookedTimes();
	}

	@Override
	public List<User> getUserByUsername(String username) {
		List<User> result = null;
		try {
			Query qu = session.createQuery("from User where username = :username");
			qu.setParameter("username", username);
			result = qu.list();
		} catch (Exception exp) {
			System.err.println("UserRepository: getUserByUsername --> "
					+ exp.getMessage());
		}
		return result;
	}

}
