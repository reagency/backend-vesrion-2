package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IRoleRepository;
import reagency.core.domain.kernel.Role;

public class RoleRepository extends Repository<Role> implements IRoleRepository {

	public RoleRepository(Class<?> type) {
		super(type);
	}

}
