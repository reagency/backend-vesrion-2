package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IApplianceRepository;
import reagency.core.domain.realestate.estateaggregate.Appliance;

public class ApplianceRepository extends Repository<Appliance> implements
		IApplianceRepository {

	public ApplianceRepository(Class<?> type) {
		super(type);
	}

}
