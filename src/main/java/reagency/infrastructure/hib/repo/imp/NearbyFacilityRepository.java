package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.INearbyFacilityRepository;
import reagency.core.domain.realestate.estateaggregate.NearbyFacility;

public class NearbyFacilityRepository extends Repository<NearbyFacility>
		implements INearbyFacilityRepository {

	public NearbyFacilityRepository(Class<?> type) {
		super(type);
	}
	

}
