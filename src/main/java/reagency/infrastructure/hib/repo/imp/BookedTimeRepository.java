package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IBookedTimeRepository;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;

public class BookedTimeRepository extends Repository<BookedTime>
		implements IBookedTimeRepository {

	public BookedTimeRepository(Class<?> type) {
		super(type);
	}

}
