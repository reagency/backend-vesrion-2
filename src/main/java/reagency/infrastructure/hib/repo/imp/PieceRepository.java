package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IPieceRepository;
import reagency.core.domain.realestate.estateaggregate.Piece;

public class PieceRepository extends Repository<Piece> implements
		IPieceRepository {

	public PieceRepository(Class<?> type) {
		super(type);
	}
	

}
