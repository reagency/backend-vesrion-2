package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IImageRepository;
import reagency.core.domain.realestate.estateaggregate.Image;

public class ImageRepository extends Repository<Image> implements
		IImageRepository {

	public ImageRepository(Class<?> type) {
		super(type);
	}

}
