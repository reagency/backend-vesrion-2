package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.IExpertAgencyRepository;
import reagency.core.domain.realestate.agantaggregate.ExpertAgency;
import reagency.core.domain.realestate.estateaggregate.Estate;

public class ExpertAgencyRepository extends Repository<ExpertAgency> implements
		IExpertAgencyRepository {

	public ExpertAgencyRepository(Class<?> type) {
		super(type);
	}
	public List<Estate> loadRepresenters(ExpertAgency expertAgency){
		Hibernate.initialize(expertAgency.getApprovals());
		return expertAgency.getApprovals();
	}

}
