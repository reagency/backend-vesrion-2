package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.IRepresenterRepository;
import reagency.core.domain.realestate.offeringaggregate.AvailableTime;
import reagency.core.domain.realestate.offeringaggregate.Representer;

public class RepresenterRepository extends Repository<Representer> implements
		IRepresenterRepository {

	public RepresenterRepository(Class<?> type) {
		super(type);
	}
	public List<AvailableTime> loadRepresenters(Representer representer){
		Hibernate.initialize(representer.getAvailableTimes());
		return representer.getAvailableTimes();
	}

}
