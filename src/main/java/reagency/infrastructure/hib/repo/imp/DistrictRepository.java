package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IDistrictRepository;
import reagency.core.domain.realestate.areaaggregate.District;

public class DistrictRepository extends Repository<District> implements
		IDistrictRepository {

	public DistrictRepository(Class<?> type) {
		super(type);
	}

}
