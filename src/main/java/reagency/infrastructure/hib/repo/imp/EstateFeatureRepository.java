package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IEstateFeatureRepository;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;

public class EstateFeatureRepository extends Repository<EstateFeature>
		implements IEstateFeatureRepository {

	public EstateFeatureRepository(Class<?> type) {
		super(type);
	}
	
}
