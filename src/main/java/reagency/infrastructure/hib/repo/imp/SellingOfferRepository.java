package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.persistence.internal.oxm.schema.model.Restriction;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;

import reagency.core.contract.infracontract.ISellingOfferRepository;
import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.Representer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;
import reagency.core.domain.realestate.typeaggregate.SellingOfferType;

public class SellingOfferRepository extends Repository<SellingOffer> implements
		ISellingOfferRepository {

	public SellingOfferRepository(Class<?> type) {
		super(type);
	}
	public List<Offer> loadOffers(SellingOffer sellingOffer){
		Hibernate.initialize(sellingOffer.getOffers());
		return sellingOffer.getOffers();
	}
	public List<Representer> loadRepresenters(SellingOffer sellingOffer){
		Hibernate.initialize(sellingOffer.getRepresenters());
		return sellingOffer.getRepresenters();
	}
	public List<SellingOffer> findByPattern(SellingOfferType type, int basePrice, String priceCondition, Date registrationDate, String registrationCondition){
		List<SellingOffer> result = new ArrayList<>();
		
		Criteria criteria = session.createCriteria(SellingOffer.class);
		if(type != null){
			criteria.add(Restrictions.eq("type", type));
		}
		
		criteria.add(Restrictions.eq("isArchived", false));
		if(priceCondition != null){
			if(!priceCondition.equals("")){
				switch (priceCondition) {
				case ">= (Greater Than or Equal)":
					criteria.add(Restrictions.ge("basePrice", basePrice));

					break;
				case "<= (Less Than or Equal)":
					criteria.add(Restrictions.le("basePrice", basePrice));
					break;
				case "= (Equal)":
					criteria.add(Restrictions.eq("basePrice", basePrice));
					break;

				default:
					break;
				}
			}
		}
		if(registrationCondition != null){
			if(!registrationCondition.equals("")){
				switch (registrationCondition) {
				case ">= (Greater Than or Equal)":
					criteria.add(Restrictions.ge("registerDate", basePrice));

					break;
				case "<= (Less Than or Equal)":
					criteria.add(Restrictions.le("registerDate", basePrice));
					break;
				case "= (Equal)":
					criteria.add(Restrictions.eq("registerDate", basePrice));
					break;

				default:
					break;
				}
			}
			
		}
		result = criteria.list();
		return result;
		
	}

}
