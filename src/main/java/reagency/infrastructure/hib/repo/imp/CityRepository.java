package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.ICityRepository;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.District;

public class CityRepository extends Repository<City> implements ICityRepository {

	public CityRepository(Class<City> type) {
		super(type);
		
	}
	public List<District> loadDistricts(City city){
		int id = city.getDistricts().size();
		//Hibernate.initialize(city.getDistricts());
		return city.getDistricts();
	}

}
