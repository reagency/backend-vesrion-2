package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import reagency.core.contract.infracontract.IStateRepository;
import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.State;

public class StateRepository extends Repository<State> implements
		IStateRepository {

	public StateRepository(Class<?> type) {
		super(type);
	}

	public List<City> loadCities(State state){
		int id = state.getCities().size();
		//Hibernate.initialize(state.getCities());
		return state.getCities();
	}

}
