package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IServerInfoRepository;
import reagency.core.domain.kernel.ServerInfo;

public class ServerInfoRepository extends Repository<ServerInfo> implements IServerInfoRepository{

	public ServerInfoRepository(Class<?> type) {
		super(type);
	}

}
