package reagency.infrastructure.hib.repo.imp;

import reagency.infrastructure.hib.Repository;
import reagency.core.contract.infracontract.IOfferRepository;
import reagency.core.domain.realestate.offeringaggregate.Offer;

public class OfferRepository extends Repository<Offer> implements
		IOfferRepository {

	public OfferRepository(Class<?> type) {
		super(type);
	}
	

}
