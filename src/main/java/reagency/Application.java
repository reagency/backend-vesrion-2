package reagency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import reagency.infrastructure.security.JwtFilter;

@ComponentScan
@Configuration
@EnableAutoConfiguration
@EnableScheduling
@SpringBootApplication
public class Application {
	@Bean
	public FilterRegistrationBean jwtFilter() {
		final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		registrationBean.setFilter(new JwtFilter());
		registrationBean.addUrlPatterns("/webapi/*");
		registrationBean.addUrlPatterns("/fileapi/*");

		return registrationBean;
	}
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
