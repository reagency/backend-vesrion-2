package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.District;

public interface ICityRepository extends IRepository<City> {
	public List<District> loadDistricts(City city);
}
