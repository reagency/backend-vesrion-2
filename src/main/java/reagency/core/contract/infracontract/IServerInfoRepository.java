package reagency.core.contract.infracontract;

import reagency.core.domain.kernel.ServerInfo;

public interface IServerInfoRepository extends IRepository<ServerInfo> {

}
