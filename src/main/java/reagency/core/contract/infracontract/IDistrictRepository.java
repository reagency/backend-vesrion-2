package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.areaaggregate.District;

public interface IDistrictRepository extends IRepository<District> {

}
