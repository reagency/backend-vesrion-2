package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.agantaggregate.ExpertAgency;
import reagency.core.domain.realestate.estateaggregate.Estate;

public interface IExpertAgencyRepository extends IRepository<ExpertAgency> {
	public List<Estate> loadRepresenters(ExpertAgency expertAgency);
}
