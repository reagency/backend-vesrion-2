package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.estateaggregate.Estate;
import reagency.core.domain.realestate.estateaggregate.EstateFeature;

public interface IEstateRepository extends IRepository<Estate> {
	public List<EstateFeature> loadRepresenters(Estate estate);
}
