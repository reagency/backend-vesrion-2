package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.offeringaggregate.BookedTime;

public interface IBookedTimeRepository extends IRepository<BookedTime> {

}
