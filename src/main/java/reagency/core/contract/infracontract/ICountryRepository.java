package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.areaaggregate.Country;
import reagency.core.domain.realestate.areaaggregate.State;

public interface ICountryRepository extends IRepository<Country> {
	public List<State> loadStates(Country country);
}
