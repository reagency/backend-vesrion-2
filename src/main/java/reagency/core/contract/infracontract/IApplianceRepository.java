package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.estateaggregate.Appliance;

public interface IApplianceRepository extends IRepository<Appliance> {

}
