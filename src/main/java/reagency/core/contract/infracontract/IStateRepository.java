package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.areaaggregate.City;
import reagency.core.domain.realestate.areaaggregate.State;

public interface IStateRepository extends IRepository<State> {
	public List<City> loadCities(State state);
}
