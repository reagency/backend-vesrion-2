package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.estateaggregate.Piece;

public interface IPieceRepository extends IRepository<Piece> {

}
