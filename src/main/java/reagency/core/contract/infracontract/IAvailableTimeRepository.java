package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.offeringaggregate.AvailableTime;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;

public interface IAvailableTimeRepository extends IRepository<AvailableTime> {
	public List<BookedTime> loadRepresenters(AvailableTime availableTime);
}
