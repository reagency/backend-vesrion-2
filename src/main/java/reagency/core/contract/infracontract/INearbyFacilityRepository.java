package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.estateaggregate.NearbyFacility;

public interface INearbyFacilityRepository extends IRepository<NearbyFacility> {

}
