package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.kernel.User;
import reagency.core.domain.realestate.offeringaggregate.BookedTime;
import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;

public interface IUserRepository extends IRepository<User> {
	public List<BookedTime> loadBookedTimes(User user);
	public List<SellingOffer> loadFavorites(User user);
	public List<Offer> loadOffers(User user);
	public List<BookedTime> loadSellingOffers(User user);
	public List<User> getUserByUsername(String username);
}
