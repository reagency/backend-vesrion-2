package reagency.core.contract.infracontract;

import java.util.Date;
import java.util.List;

import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.Representer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;
import reagency.core.domain.realestate.typeaggregate.SellingOfferType;

public interface ISellingOfferRepository extends IRepository<SellingOffer> {
	public List<Offer> loadOffers(SellingOffer sellingOffer);
	public List<Representer> loadRepresenters(SellingOffer sellingOffer);
	public List<SellingOffer> findByPattern(SellingOfferType type, int basePrice, String priceCondition, Date registrationDate, String registrationCondition);
}
