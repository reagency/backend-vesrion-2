package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.estateaggregate.EstateFeature;

public interface IEstateFeatureRepository extends IRepository<EstateFeature> {

}
