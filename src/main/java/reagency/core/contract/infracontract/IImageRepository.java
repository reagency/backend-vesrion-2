package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.estateaggregate.Image;

public interface IImageRepository extends IRepository<Image> {

}
