package reagency.core.contract.infracontract;

import reagency.core.domain.kernel.Role;

public interface IRoleRepository extends IRepository<Role> {

}
