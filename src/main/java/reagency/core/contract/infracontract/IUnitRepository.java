package reagency.core.contract.infracontract;

import java.util.List;

import reagency.core.domain.realestate.estateaggregate.Appliance;
import reagency.core.domain.realestate.estateaggregate.Image;
import reagency.core.domain.realestate.estateaggregate.Piece;
import reagency.core.domain.realestate.estateaggregate.Unit;

public interface IUnitRepository extends IRepository<Unit> {
	public List<Appliance> loadAppliances(Unit unit);
	public List<Image> loadImages(Unit unit);
	public List<Piece> loadPieces(Unit unit);
}
