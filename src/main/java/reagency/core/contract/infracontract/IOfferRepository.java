package reagency.core.contract.infracontract;

import reagency.core.domain.realestate.offeringaggregate.Offer;

public interface IOfferRepository extends IRepository<Offer> {

}
