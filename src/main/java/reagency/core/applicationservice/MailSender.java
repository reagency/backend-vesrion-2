package reagency.core.applicationservice;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.ServerInfo;

public class MailSender {
	public boolean sendEmail(String message, String reciverEmail, String subject) {
		boolean result = false;
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		ServerInfo info = uow.getServerInfoRepository().findById(1);

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(info.getEmail(), info
								.getPassword());
					}
				});
		uow.commit();
		context.close();
		try {
			Message sender_message = new MimeMessage(session);
			sender_message
					.setFrom(new InternetAddress(info.getEmail()));
			sender_message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(reciverEmail));
			sender_message.setSubject(subject);
			sender_message.setText(message);

			Transport.send(sender_message);
			result = true;
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
}
