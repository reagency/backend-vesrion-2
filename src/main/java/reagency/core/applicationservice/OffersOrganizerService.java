package reagency.core.applicationservice;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.ServerInfo;
import reagency.core.domain.realestate.offeringaggregate.Offer;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;

public class OffersOrganizerService implements Runnable{

	@Override
	public void run() {
		while (true) {
			AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
					DIConfiguration.class);
			IUnitOfWork uow = context.getBean(UnitOfWork.class);
			ServerInfo info = uow.getServerInfoRepository().findById(1);
			uow.commit();
			context.close();
			int offerExpiry;
			if (info != null) {
				offerExpiry = info.getOfferExpiry();
			} else {
				offerExpiry = 3;
			}
			List<Offer> offers = uow.getOfferRepository()
					.findAll();
			for (Offer offer : offers) {
				Date registerDate = offer.getRegisterDate();
				Date actualDate = new Date();

				long diffDays = (actualDate.getTime() - registerDate.getTime())
						/ (24 * 60 * 60 * 1000);
				if (diffDays > offerExpiry) {
					offer.setIsArchived(true);
					offer.setDateOfArchive(actualDate);
				}
			}
			int sleeptime = offerExpiry * 24 * 60 * 60 * 1000;
			try {
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

}
