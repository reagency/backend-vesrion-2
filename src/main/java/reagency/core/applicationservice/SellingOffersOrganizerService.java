package reagency.core.applicationservice;

import reagency.infrastructure.hib.DIConfiguration;
import reagency.infrastructure.hib.UnitOfWork;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import reagency.core.contract.infracontract.IUnitOfWork;
import reagency.core.domain.kernel.ServerInfo;
import reagency.core.domain.realestate.offeringaggregate.SellingOffer;

public class SellingOffersOrganizerService implements Runnable {

	@Override
	public void run() {
		while (true) {
			AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
					DIConfiguration.class);
			IUnitOfWork uow = context.getBean(UnitOfWork.class);
			ServerInfo info = uow.getServerInfoRepository().findById(1);
			uow.commit();
			context.close();
			int sellingOfferExpiry;
			if (info != null) {
				sellingOfferExpiry = info.getSellingExpiry();
			} else {
				sellingOfferExpiry = 15;
			}
			List<SellingOffer> sellingOffers = uow.getSellingOfferRepository()
					.findAll();
			for (SellingOffer sellingOffer : sellingOffers) {
				if(sellingOffer.getDateOfArchive() != null && sellingOffer.getIsSold()){
					Date registerDate = sellingOffer.getDateOfArchive();
					Date actualDate = new Date();

					long diffDays = (actualDate.getTime() - registerDate.getTime())
							/ (24 * 60 * 60 * 1000);
					if (diffDays > sellingOfferExpiry) {
						sellingOffer.setIsArchived(true);
						//sellingOffer.setDateOfArchive(actualDate);
					}
				}
				
			}
			int sleeptime = sellingOfferExpiry * 24 * 60 * 60 * 1000;
			try {
				Thread.sleep(sleeptime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

}
