package reagency.core.domain.realestate.estateaggregate;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.mysql.jdbc.Blob;

import reagency.core.domain.kernel.IArchivable;
import reagency.core.domain.kernel.IEntity;
import reagency.core.domain.realestate.typeaggregate.ImageType;

@XmlRootElement
public class Image implements IEntity, IArchivable {

	private int id;
    private int version;
    
	private boolean isArchived;
    private Date dateOfArchive;
    private ImageType type;
    private String imageData;
    private Unit owner;
    private Estate estateOwner;
    
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public boolean getIsArchived() {
		return isArchived;
	}
	public void setIsArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}
	public Date getDateOfArchive() {
		return dateOfArchive;
	}
	public void setDateOfArchive(Date dateOfArchive) {
		this.dateOfArchive = dateOfArchive;
	}
	public ImageType getType() {
		return type;
	}
	public void setType(ImageType type) {
		this.type = type;
	}
	
	public Unit getOwner() {
		return owner;
	}
	public void setOwner(Unit owner) {
		this.owner = owner;
	}
	public Estate getEstateOwner() {
		return estateOwner;
	}
	public void setEstateOwner(Estate estateOwner) {
		this.estateOwner = estateOwner;
	}
	public String getImageData() {
		return imageData;
	}
	public void setImageData(String imageData) {
		this.imageData = imageData;
	}
	
}
