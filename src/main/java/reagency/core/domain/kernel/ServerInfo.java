package reagency.core.domain.kernel;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServerInfo implements IEntity, IArchivable {
	private int id;
    private int version;
    
	private boolean isArchived;
	
	private Date dateOfArchive;
	
	private String email;
	private String password;
	private int offerExpiry;
	private int sellingExpiry;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getVersion() {
		return version;
	}
	public void setVersion(int version) {
		this.version = version;
	}
	public boolean getIsArchived() {
		return isArchived;
	}
	public void setIsArchived(boolean isArchived) {
		this.isArchived = isArchived;
	}
	public Date getDateOfArchive() {
		return dateOfArchive;
	}
	public void setDateOfArchive(Date dateOfArchive) {
		this.dateOfArchive = dateOfArchive;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getOfferExpiry() {
		return offerExpiry;
	}
	public void setOfferExpiry(int offerExpiry) {
		this.offerExpiry = offerExpiry;
	}
	public int getSellingExpiry() {
		return sellingExpiry;
	}
	public void setSellingExpiry(int sellingExpiry) {
		this.sellingExpiry = sellingExpiry;
	}
}
